var http = require('http');
var fs = require('fs');

http.createServer(function(req, res) {
    if (req.headers.accept && req.headers.accept == 'text/event-stream') {
        if (req.url == '/sse') {
            push(req, res);
        } else {
            res.writeHead(404);
            res.end();
        }
    } else {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(fs.readFileSync(__dirname + '/client.html'));
        res.end();
    }
}).listen(3000);

function push(req, res){
    setInterval(function() {
        push(req, res);
    }, 2000);
    
    res.writeHead(200, {
        'Content-Type': 'text/event-stream',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive'
    });
    
    res.write('data: ' + Date() + '\n\n');
}