mkdir sse
git clone https://davidjbarnes@bitbucket.org/davidjbarnes/nodejs-server-sent-events.git sse
cd sse
node server.js

Open browser to: http://localhost:3000/client.html

Client uses EventSource("http://localhost:3000/sse") to “subscribe” to server sent events.
